
You can generate these examples with:

   % pmaker -d MyTest.data.xml @examples/hltsv_only

Typical options are:

     -Dlocalhost=1                   for a localhost version
     -Ddatafile=/path/to/rawfile     if you need pre-loaded data
     -Dpartition=NAME                if you want a different partition name 'NAME'
     -Dros-segment=/path/to/segment  if you want to include a pre-generated ROS segment 
                                     (use ros/include.pm instead of emulated or preloaded)
     -Dsite=standalone               if you are on a non-standard system.

hltsv_only
        HLTSV plus dummy DCM and ROS apps.

hltsv_dcm
        HLTSV plus DCM using dummy for DC, output, processor.

hltsv_dcm_sfo
        HLTSV plus DCM plus SFO, DCM using dummy for DC and processor.

dcm_only
        DCM only with all dummies.

hltpu_only
        HLTPU only with dummy DF interface.

ros_only
        ROS segment only generation

hlt_segment
        HLT segment only generation 

full_partition
        Full partition (HLTSV, DCM, HLTMPPU, SFO, ROS)


=========================================================================================

If you have copied the OKS databases from a different site to a local directory, you can
also modify your TDAQ_DB_PATH variable and try the generation for a site which you are currently not
logged on:

     export TDAQ_DB_PATH=/path/to/point1/db:$TDAQ_DB_PATH

     pmaker -d MyTest.data.xml @examples/full_partition -Dsite=point1

==========================================================================================

You can always add more modules on the command line to fix any remaining issues that
are specific for your use case:

    pmaker -d MyTest.data.xml @examples/full_partition mymodule.pm

or insert your modules into a copy of the original project file.

A few modules will try to include a user module if it exists. These are:

site.pm
   - will include site/pre-user.pm before it does anything.
   - will include site/post-user.pm after it is done.

partition.pm
   - will include partition/pre-user.pm before it does anything
   - will include partition/post-user.pm after it is done.

