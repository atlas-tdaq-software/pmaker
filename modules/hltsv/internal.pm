# -*- Python -*-
include('daq/schema/hltsv.schema.xml')
include('daq/sw/repository.data.xml')

extend(context, {
        'HLTSVApplication': {
            'RoIBInput':  RoIBPluginInternal('internal',Libraries=['libsvl1internal'])
            }
        })
