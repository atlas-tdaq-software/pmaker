# -*- Python -*-
include('daq/schema/hltsv.schema.xml')
include('daq/sw/repository.data.xml')

extend(context, {
        'HLTSVApplication': {
            'RoIBInput':  RoIBPluginFilar('filar',Libraries=['libsvl1filar'])
            }
        })
