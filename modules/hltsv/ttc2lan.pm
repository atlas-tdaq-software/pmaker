# -*- Python -*-
include('daq/schema/hltsv.schema.xml')
include('daq/sw/repository.data.xml')

extend(context, {
        'HLTSVApplication': {
            'RoIBInput':  RoIBPluginTTC2LAN('ttc2lan',Libraries=['libsvl1ttc2lan'],Networks=['137.138.0.0/255.255.0.0'])
            }
        })
