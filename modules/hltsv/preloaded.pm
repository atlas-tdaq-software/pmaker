# -*- Python -*-
include('daq/schema/hltsv.schema.xml')
include('daq/sw/repository.data.xml')

extend(context, {
        'HLTSVApplication': {
            'RoIBInput':  RoIBPluginPreload('preloaded',Libraries=['libsvl1preloaded'])
            }
        })
