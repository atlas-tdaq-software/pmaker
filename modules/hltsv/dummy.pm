# -*- Python -*-
#
# Generate applications for running HLTSV with dummy DCM and ROS
#
include('daq/schema/hltsv.schema.xml')
include('daq/sw/common-templates.data.xml')

extend_list(context, {

        'HLTTemplateSegment': {
            'Applications': [ HLTSV_DCMTest('dcm', Program=Binary.testDCM, Instances=8) ]
            },

        'Partition': {
            'Segments': [ Segment('ROS', 
                                  IsControlledBy=RunControlTemplateApplication.DefRC,
                                  Applications=[RunControlApplication('ROS-1',
                                                                      Program=Binary.testROS)] 
                                  )
                          ]
            }
        })
