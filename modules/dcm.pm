# -*- Python -*-

include('daq/schema/dcm.schema.xml')
include('daq/sw/repository.data.xml')

require('monsvc.pm')

#
# The DCM application itself
#
# l1Source, processor, dataCollector, output should be set by other modules
# and registered under 'DCM'.
#
DcmApplication('dcm', 
               Program=Binary.dcm_main
              )

update(DcmApplication.dcm, context.get('DCM', {}))
update(DcmApplication.dcm, context.get('DFMonsvc', {}))

extend_list(context, {
        'HLTTemplateSegment': {
            'Applications': [ DcmApplication.dcm ]
            }
})
