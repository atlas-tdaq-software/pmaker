# -*- Python -*-
#
# Try to auto-detect the current 'site' and require
# the proper site-XXX.pm file.
#

sites = [
    ('testbed', ['pc-tbed-.*', 'vm-tbed-.*', 'sbc-tbed-.*']),
    ('preseries',['pc-preseries-.*', 'vm-preseries-.*', 'sbc-preseries-.*']),
    ('point1',  ['pc-atlas-.*', 'pc-tdq-.*', 'vm-atlas-.*', 'vm-tdq-.*', 'sbc-.*']),
    ('lxplus',['lxplus.*']),
    ('standalone', ['.*'])
]

def find_site():

    import socket
    from re import match
    global sites

    me = socket.getfqdn()

    for s in sites:
        for pattern in s[1]:
            if match(pattern, me):
                return s[0]

try:
    require('site/pre.pm')
    logger.info('site/pre.pm module executed')
except:
    logger.info('No site/pre.pm module found')

site_name = context.get('site', find_site())

if site_name:
    try:
       require('site/' + site_name + '.pm')
       logger.info("Identified site as: %s",site_name)
    except:
       logger.error("Could not find site specific module: %s", 'site/' + site_name + '.pm')
       raise
else:
    logger.error("Could not identify site")

if context.get('localhost'):
    require('localhost.pm')

try:
    require('site/post.pm')
    logger.info('site/post.pm module executed')
except:
    logger.info('No site/post.pm module found')
