# -*- Python -*-

include('daq/schema/HLTMPPU.schema.xml')

extend(context, {
        'Partition': {
            'TriggerConfiguration': {
                'hlt': HLTImplementationJobOptions('HLTTrigger',
                                                   libraries=["TrigPSC", "TrigServices"],
                                                   HLTCommonParameters=HLTCommonParameters('HLTCommonParams'))
                }
            }
        })

