# -*- Python -*-

include('daq/schema/pudummy.schema.xml')

extend(context, {
        'Partition': {
            'TriggerConfiguration': {
                'hlt': PuDummySteering('HLTDummy', 
                                       libraries=["pudummy"],
                                       steps=[DummyStep('step-0',
                                                        burnTimeDistribution="C|1",
                                                        physicsTag=DummyStreamTag('first'),
                                                        debugTag=DummyStreamTag('debug'))])
                }
            }
        })

