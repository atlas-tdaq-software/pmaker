# -*- Python -*-

include('daq/schema/df.schema.xml')
include('daq/sw/repository.data.xml')
include('daq/sw/common-templates.data.xml')

# Build the ROS applications based on the list in 'ROS'
# and the common configuration in 'ROSCommon'
#
c = counter()
roses = []
for ros_config in context.get('ROS', []):
    ros = ROS('ROS-%d' % c(), Program=Binary.ReadoutApplication)
    update(ros, context.get('ROSCommon', {}))
    update(ros, ros_config)
    roses.append(ros)

extend_list(context, {
        'ROSSegment': {
            'Resources': roses
            }
        })

#
# Build the ROS segment based on 'ROSSegment' context information
#
Segment('ROS',
        Infrastructure = [ IPCServiceTemplateApplication.DefRDB ],
        IsControlledBy = RunControlTemplateApplication.DefRC,
        )

update(Segment.ROS, context.get('ROSSegment', {}))

extend_list(context, {
        'Partition': {
            'Segments': [ Segment.ROS ]
            }
        })
