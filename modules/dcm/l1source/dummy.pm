# -*- Python -*-

include('daq/schema/dcm.schema.xml')

extend(context, {
        'DCM': {
            'l1Source': DcmDummyL1Source('dcml1source')
            }
        })
