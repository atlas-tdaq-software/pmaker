# -*- Python -*-

include('daq/schema/dcm.schema.xml')

extend(context, {
        'DCM': {
            'output': DcmSfoEfioOutput('dcmOutput',
                                       EfioConfiguration=EFIOConfiguration("EFIOConfiguration"))
            },
        'SFOConfiguration': {
            'EFIOConfiguration': EFIOConfiguration.EFIOConfiguration
            }
        })
