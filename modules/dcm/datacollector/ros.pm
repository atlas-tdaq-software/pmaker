# -*- Python -*-

include('daq/schema/dcm.schema.xml')

extend(context, {
        'DCM': {
            'dataCollector': DcmRosDataCollector('dcmDataCollector')
            }
        })
