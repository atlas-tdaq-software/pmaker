# -*- Python -*-

include('daq/schema/dcm.schema.xml')

extend(context, {
        'DCM': {
            'processor': DcmHltpuProcessor('dcmProcessor')
            }
        })
