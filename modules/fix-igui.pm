# -*- Python -*-

# Fix the publishing parameters for IGUI until there is a patch

include('daq/schema/core.schema.xml')

extend(context, {
        'Partition': {
            'IS_InformationSource': IS_InformationSources('counters',
                                                          LVL1=IS_EventsAndRates('lvl1',
                                                                                 EventCounter='DF.HLTSV.Events.LVL1Events'
                                                                                 ),
                                                          HLT=IS_EventsAndRates('hlt',
                                                                                EventCounter='DF.HLTSV.Events.ProcessedEvents',
                                                                                Rate='DF.HLTSV.Events.Rate'
                                                                                )
                                                         )
            }
        })
