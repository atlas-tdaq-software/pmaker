# -*- Python -*-

if 'ros-segment' not in context:
    logger.critical('This module requires a command line parameter: -Dros-segment=<ROS Segment File>')

include(context.get('ros-segment'))

extend_list(context, {
        'Partition': {
            'Segments': [ seg for seg in Segment if seg.id.startswith('ROS-') ]
            }
})

