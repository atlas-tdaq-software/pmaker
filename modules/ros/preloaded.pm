# -*- Python -*-
include('daq/schema/df.schema.xml')
include('daq/schema/DFTriggerIn.schema.xml')

#
# Preloaded ROS configuration for one single ROS holding all data.
#
extend(context, {
        'ROSCommon': {
            'Configuration': ReadoutConfiguration('ROSReadoutConfiguration'),
            'MemoryConfiguration': MemoryPool('ROSMemoryPool',
                                              Type = 'MALLOC',
                                              NumberOfPages = 32,
                                              PageSize = 4096),
            'Trigger': [ DFTriggerIn('ROSTriggerIn')] , 
            'Detector': Detector('FULL_EVENT')
            }
        })

try:
    import robhit
    channels = robhit.robhit
except:
    logger.warning('No robhit.py module found, creating empty ROS')
    channels = []

extend(context, {
        # list of ROS descriptions, should typically contain
        # { 'RunsOn': ..., 'Contains': [ ... ] }
        'ROS': [ { 'RunsOn': None, 
                   'Contains': [ PreloadedReadoutModule('PreloadedModule',
#                                                        Configuration=ReadoutConfiguration('ROSModuleConfiguration'),
                                                        Contains=[ InputChannel("ROSChannel-0x%06x" % chan, 
                                                                                Id=chan, 
                                                                                MemoryConfiguration=MemoryPool.ROSMemoryPool) for chan in channels 
                                                                  ]) ] } ]
        })

