# -*- Python -*-
include('daq/schema/df.schema.xml')
include('daq/schema/DFTriggerIn.schema.xml')

#
# Emulated ROS configuration for one single ROS holding all data.
#
extend(context, {
        'ROSCommon': {
            'Configuration': ReadoutConfiguration('ROSReadoutConfiguration'),
            'MemoryConfiguration': MemoryPool('ROSMemoryPool',
                                              Type = 'MALLOC',
                                              NumberOfPages = 32,
                                              PageSize = 4096),
            'Trigger': [ DFTriggerIn('ROSTriggerIn')] , 
            'Detector': Detector('PIXEL_BARREL',LogicalId=17)
            }
        })

extend(context, {
        # list of ROS descriptions, should typically contain
        # { 'RunsOn': ..., 'Contains': [ ... ] }
        'ROS': [ { 'RunsOn': None, 
                   'Contains': [ EmulatedReadoutModule('EmulatedModule',
                                                       Configuration=EmulatedReadoutConfiguration('ROSModuleConfiguration'),
                                                       Contains=[ InputChannel("ROSChannel-0x%06x" % chan, 
                                                                               Id=chan, 
                                                                               MemoryConfiguration=MemoryPool.ROSMemoryPool) for chan in range(111705,111721) 
                                                                  ]) ] } ]
        })

