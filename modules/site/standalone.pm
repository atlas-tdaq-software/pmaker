from __future__ import print_function
# -*- Python -*-

import string

info = None

try:
   include('daq/hw/hosts.data.xml')
   logger.info("Included a daq/hw/hosts.data.xml file from TDAQ_DB_PATH")
except:
   logger.info("Could not find a standard daq/hw/hosts.data.xml file")

try :
   from netifaces import AF_INET, interfaces, ifaddresses

   addr = ifaddresses(interfaces()[1])[AF_INET]
   info = addr[0]['addr'] + '/' + addr[0]['netmask']
except ImportError:
   import socket
   info = socket.gethostbyname(socket.getfqdn()) + '/255.255.0.0'
   logger.error('Please check netmask in DFParameters, set to 255.255.0.0')

if info:
   extend(context,{
         'Partition': {
            'DataFlowParameters': {
               'DefaultDataNetworks': [ info ]
               }
            },
         'SFOConfiguration': {
            'DataDirectories': [ '/tmp' ]
            }
         })
else:
   print("Warning: no network info found")
   
