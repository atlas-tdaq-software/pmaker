# -*- Python -*-

include('daq/hw/lxplus-cluster.data.xml')

extend(context, {
   'Partition': {
        'DataFlowParameters': {
            'DefaultDataNetworks': [ '137.138.0.0/255.255.0.0', '188.184.0.0/255.255.0.0' ]
        }
    }
})
