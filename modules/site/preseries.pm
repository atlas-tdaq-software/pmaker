# -*- Python -*-

include('daq/hw/hosts.data.xml')

extend(context, {
   'Partition': {
       'DefaultHost': Computer['pc-preseries-onl-01.cern.ch'],
        'LogRoot': '/logs/${TDAQ_VERSION}',
        'WorkingDirectory': '/logs/${TDAQ_VERSION}' ,
        'DataFlowParameters': {
            'DefaultDataNetworks': [ '10.147.0.0/255.255.0.0', '10.150.0.0/255.255.0.0' ]
        }
    },
   'HLTSVApplication' : {
       'RunsOn': Computer['pc-preseries-l2sv-01.cern.ch']
   }
})
