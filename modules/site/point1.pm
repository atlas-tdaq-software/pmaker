# -*- Python -*-

import random

include('daq/schema/core.schema.xml')
include('daq/hw/hosts.data.xml')
include('daq/segments/HLT/SFODBConnection.data.xml')

#
# A pre-defined table of multicast addresses, selected by context['detector'] on the command line.
#
multicast_addresses = {
    # 224.100.2.* for user partitions, randomly assigned
    # 224.100.4.* for global
    'ATLAS':  '224.100.4.1/10.147.0.0',
    # TDAQ
    'TDAQ':  '224.100.5.1/10.147.0.0',
    # 224.100.6.* for detectors
    'Pixel': '224.100.6.1/10.147.0.0',
    'SCT'  : '224.100.7.1/10.147.0.0',
    'TRT'  : '224.100.8.1/10.147.0.0',
    'LAr':   '224.100.9.1/10.147.0.0',
    'Tile':  '224.100.10./10.147.0.0',
    'MDT':  '224.100.11.1/10.147.0.0',
    'RPC':  '224.100.12.1/10.147.0.0',
    'TGC':  '224.100.13.1/10.147.0.0',
    'CSC':  '224.100.14.1/10.147.0.0',
    'Lucid': '224.100.15.1/10.147.0.0',
    'ALFA': '224.100.16.1/10.147.0.0',
}

extend(context, {
        'HLTSVApplication' : {
            'RunsOn': Computer['pc-tdq-dc-02.cern.ch']
            },

        'Partition': {
            'LogRoot': '/logs/${TDAQ_VERSION}',
            'WorkingDirectory': '/logs/${TDAQ_VERSION}' ,
            'DefaultHost': Computer['pc-tdq-onl-80.cern.ch'],
            'DataFlowParameters': {
                'DefaultDataNetworks': [ '10.147.0.0/255.255.0.0', '10.150.0.0/255.255.0.0' ],
                'MulticastAddress': multicast_addresses.get(context.get('detector',None),'224.100.2.%d/10.147.0.0' % random.choice(range(1,255)))
                }
            },

        'SFO': sorted([ { 'RunsOn': sfo } for sfo in Computer if sfo.id.startswith('pc-tdq-sfo-') ], key=lambda s: s['RunsOn'].id),

        'SFOConfiguration': {
            'BufferSize_kB': 30000,
            'LumiBlockTimeout': 5,
            'DataDirectories': [
                 "/raid_cntl0/data",
                 "/raid_cntl1/data",
                 "/raid_cntl2/data",
                ],
            'DirectoryWritingTime': 60,
            'DirectoryChangeTime': 15,
            'MaxMegaBytesPerFile': 5000,
            'DirectoryToWriteIndex': '/tmp',
            # 'SFODBConnection': SFODBConnection.SFODBwSqlite
            },

        'HLTTemplateSegment': {
            'Racks': sorted([ rack for rack in Rack if rack.id.startswith('xpu-rack-') or rack.id.startswith('ef-rack-') ],key=lambda r: r.id)
            }
        })
