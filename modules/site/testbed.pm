# -*- Python -*-

import random

include('daq/hw/hosts.data.xml')
include('daq/schema/SFOng.schema.xml')

extend(context, {
        'Partition': {
            'DataFlowParameters': {
                'DefaultDataNetworks': [ '10.193.64.0/255.255.254.0', '10.193.128.0/255.255.254.0' ],
                'MulticastAddress': '224.100.1.%d/10.193.64.0' % random.choice(range(2,255))
                },
            'LogRoot': '/logs',
            'WorkingDirectory': '/logs',
            'DefaultHost': Computer['vm-tbed-onl-02.cern.ch']
            },

        'HLTSVApplication': {
            'RunsOn': Computer['pc-tbed-r3-01.cern.ch'],
            },

        'SFO': [ { 'RunsOn': host } for host in Computer if host.id in ['pc-tbed-r3-02.cern.ch',
                                                                        'pc-tbed-r3-03.cern.ch',
                                                                        'pc-tbed-r3-04.cern.ch',
                                                                        'pc-tbed-r3-05.cern.ch',
                                                                        'pc-tbed-r3-06.cern.ch'] 
                 ],
        
        'SFOConfiguration': {
            'DataDirectories': [ '/tmp' ]
            },

        'HLTTemplateSegment': {
            'Racks' : sorted([ rack for rack in Rack if rack.id.startswith('tpu-rack-') ], key=lambda r: r.id)
            }
        })
