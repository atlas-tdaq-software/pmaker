# -*- Python -*-
# A new partition object is generated.
# The name of the partition is taken from the 'partition' variable or 'dbname', in this order.
# 
# These are passed in from the main program in the 'context'. While 'dbname' is based on the
# database name, 'partition' has to be passed explicitly with 'pmaker ... -Dpartition=myname ...'
#
# A 'default' DFParameters object is generated.
#

include('daq/segments/setup.data.xml')
include('daq/schema/df.schema.xml')

DFParameters('default')

try:
    require('partition/pre.pm')
    logger.info('partition/pre.pm module executed')
except:
    logger.info('No partition/pre.pm module found')


partition = context.get('partition', None) or context['dbname']

if context.get('Partition',{}).get('MasterTrigger',{}).get('Controller'):
    masterTrigger = MasterTrigger('MasterTrigger')
else:
    masterTrigger = None

Partition(partition, 
          OnlineInfrastructure=OnlineSegment.setup, 
          DataFlowParameters=DFParameters.default,
          DefaultTags = [ Tag['x86_64-slc6-gcc8-opt'], Tag['x86_64-slc6-gcc8-dbg'], Tag['x86_64-centos7-gcc8-opt'] ],
          ProcessEnvironment = [ VariableSet.CommonEnvironment , VariableSet['External-environment'] ],
          Parameters = [ VariableSet.CommonParameters, VariableSet['External-parameters']  ],
          TriggerConfiguration=TriggerConfiguration('TriggerConfiguration'),
          MasterTrigger=masterTrigger
          )

update(Partition[partition], context.get('Partition', {}))

try:
    require('partition/post.pm')
    logger.info('partition/post.pm module executed')
except:
    logger.info('No partition/post.pm module found')
