# -*- Python -*-

include('daq/schema/HLTMPPU.schema.xml')

#
# Plugin for HLTPU dummy backend (i.e. no DCM)
#
extend(context, {
        'HLTRC': {
            'DataSource': HLTDFDummyBackend('dfbackend', 
                                            library="DFDummyBackend")
            }
        })

