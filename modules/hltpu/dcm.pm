# -*- Python -*-

include('daq/schema/HLTMPPU.schema.xml')

#
# Plugin for HLTPU DCM back-end
#
extend(context, {
        'HLTRC': {
            'DataSource': HLTDFDCMBackend('dfbackend', library='dfinterfaceDcm')
            }
        })

