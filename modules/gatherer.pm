# -*- Python -*-

include('daq/sw/common-templates.data.xml')

extend_list(context, {
        'HLTSegment' : {
            'Resources': [ MIGApplication['TopMIG-IS'], MIGApplication['TopMIG-OH'] ],
            'Infrastructure': [ IPCServiceTemplateApplication.DF_IS, IPCServiceTemplateApplication.DF_Histogramming ]
            },
        'HLTTemplateSegment': {
            'Resources': [ MIGApplication['DefMIG-IS'], MIGApplication['DefMIG-OH'] ],
            'Infrastructure': [ IPCServiceTemplateApplication.DF_IS, IPCServiceTemplateApplication.DF_Histogramming, IPCServiceTemplateApplication.DefRDB ]
            }
        })
