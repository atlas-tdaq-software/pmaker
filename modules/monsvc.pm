# -*- Python -*-

include('daq/schema/monsvc_config.schema.xml')

# configuration parameters

ConfigurationRuleBundle('Default_Rules',
                        Rules=[ ConfigurationRule('Default_ISRule',
                                                  Parameters=ISPublishingParameters('Default_IS',
                                                                                    ISServer="${TDAQ_IS_SERVER=DF}", 
                                                                                    PublishInterval=5)),
                                ConfigurationRule('Default_OHRule',
                                                  Parameters=OHPublishingParameters('Default_OH',
                                                                                    OHServer="${TDAQ_OH_SERVER=Histogramming}",
                                                                                    ROOTProvider="${TDAQ_APPLICATION_NAME}",
                                                                                    PublishInterval=20))
                                ])

extend(context, {
        'DFMonsvc' : {
            'ConfigurationRules': ConfigurationRuleBundle.Default_Rules
            }
})
