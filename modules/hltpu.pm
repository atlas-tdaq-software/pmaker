# -*- Python -*-

include('daq/schema/HLTMPPU.schema.xml')
include('daq/sw/repository.data.xml')

require('monsvc.pm')

#
# Create HLTRC and HLTMPPU applications and prepare for them
# to be inserted into the HLTTemplateSegment.
#
# The HLTRC.DataSource relation has to be set by another module.
#

HLTRCApplication('HLTRC', 
                 Program=Binary.HLTRC_main,
                 InfoService=HLTMonInfoImpl('hltmppu_monsvc'))

HLTMPPUApplication('HLTMPPU', 
                   Program=Binary.HLTMPPU_main)

update(HLTRCApplication.HLTRC, context.get('HLTRC', {}))
update(HLTRCApplication.HLTRC.InfoService, context.get('DFMonsvc',{}))

extend_list(context, {
        'HLTTemplateSegment': {
            'Applications': [ HLTRCApplication.HLTRC, HLTMPPUApplication.HLTMPPU ]
            }
})
