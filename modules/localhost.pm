# -*- Python -*-

# This uses more black magic than anybody else should need.
# If the local computer is not describe in any of the databases
# already loaded, a new Computer object is generated for it.
#
# This is made the default host 
# for the partition, and a dummy rack with just that node
# is generated for the HLT segment.
#

include('daq/schema/core.schema.xml')

import socket
import pm.farm

me = socket.getfqdn()
if not me in Computer:
   localhost = pm.farm.local_computer()
   db.add_dal(localhost)
else:
   localhost = Computer[me]
   
Rack('local')
Rack.local.LFS = [ localhost ]
Rack.local.Nodes = [ localhost, localhost ]

require('site/standalone.pm')

extend(context,({
         'Partition': { 
            'DefaultHost': None,
            'DataFlowParameters': {
               'MulticastAddress': ''
               },
            },
         'HLTSVApplication': {
            'RunsOn': None
            },
         'SFO': [ { 'RunsOn': None } ],
         'HLTTemplateSegment' : {
            'Racks': [ Rack.local ]
            }
         }))
