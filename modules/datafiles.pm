# -*- Python -*-

#
# Example for specifying a list of data files for later use.
# This uses two parameter from the command line as input
#
# -Ddatafile=<filename> for a single file
# -Ddatafiles=<filename> where each line in the <filename> specifies a datafile
#
# Or, in a private setup, just insert DataFile() objects with the correct path into
# the 'data_files' variable here.
#
import os.path

include('daq/schema/df.schema.xml')

data_files = []
c = counter()

if 'datafile' in context:
    data_files = [ DataFile('data-%02d' % c(), FileName=context['datafile']) ]

if 'datafiles' in context:
    data_files.extend([ DataFile('data-%02d' % c(), FileName=name) for name in open(context['datafiles']).readlines() ])

if data_files:
    extend(context, {
            'Partition': {
                'DataFlowParameters' : {
                    'UsesDataFiles': data_files
                    }
                }
            })
