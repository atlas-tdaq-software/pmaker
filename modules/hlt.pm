# -*- Python -*-

include('daq/schema/df.schema.xml')
include('daq/schema/dcm.schema.xml')
include('daq/sw/common-templates.data.xml')

TemplateSegment('hlt')
TemplateSegment.hlt.IsControlledBy = RunControlTemplateApplication.DefRC

Segment('HLT')
Segment.HLT.IsControlledBy = RunControlTemplateApplication.DefRC
Segment.HLT.Segments = [ TemplateSegment.hlt ] 

update(Segment.HLT, context.get('HLTSegment', {}))
update(TemplateSegment.hlt, context.get('HLTTemplateSegment', {}))

extend_list(context, { 
        'Partition': { 
            'Segments' : [ Segment.HLT ] }
        })
