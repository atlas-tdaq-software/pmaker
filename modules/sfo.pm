# -*- Python -*-

include('daq/schema/SFOng.schema.xml')
require('monsvc.pm')

# SFO itself...

# Customize global SFOConfiguration
config = SFOngConfiguration('SFOConfiguration')
update(config, context.get('SFOConfiguration', {}))

c = counter()

sfos = [  update(SFOngApplication('SFO-%d' % c(),
                                  Program=Binary.SFOng_main,
                                  SFOngConfiguration=config), sfo_app)
          for sfo_app in context.get('SFO',[]) ]

for sfo in sfos:
    update(sfo, context.get('DFMonsvc',{}))

extend_list(context, {
        'HLTSegment': {
            'Resources':  sfos
            }
        })
