# -*- Python -*-

#
# Create the HLTSV with its publishing parameters (hardcoded for 'DF' and 'Histogramming')
# Customize RoIBInput via context, or default to internal mode.

include('daq/schema/hltsv.schema.xml')
include('daq/sw/repository.data.xml')

# Configuration parameters to publish everything to DF, not a rack level server
ConfigurationRuleBundle('HLTSV_Rules',
                        Rules = [ ConfigurationRule('HLTSV_ISRule',Parameters=ISPublishingParameters('HLTSV_IS',ISServer="DF", PublishInterval=2)),
                                  ConfigurationRule('HLTSV_OHRule',Parameters=OHPublishingParameters('HLTSV_OH',OHServer="Histogramming",ROOTProvider="${TDAQ_APPLICATION_NAME}",PublishInterval=20))
                                  ]
                        )

HLTSVApplication('HLTSV',Program=Binary.hltsv_main,ConfigurationRules=ConfigurationRuleBundle.HLTSV_Rules)

# apply context customizations
update(HLTSVApplication.HLTSV, context.get('HLTSVApplication',{}))

extend_list(context, {
        'HLTSegment': {
            'Resources': [ HLTSVApplication.HLTSV ]
            }
        })



if False and isinstance(HLTSVApplication.HLTSV.RoIBInput, RoIBMasterTriggerPlugin):
    extend(context, {
            'Partition': {
                'MasterTrigger': {
                    'Controller': HLTSVApplication.HLTSV
                    }
                }
            })
