
pmaker: PartitionMaker for the Masses
------------------------------------

This script attempts to solve several problems with 
the current pm_set.py utility and programming in the
partition maker environment in general.

It provides a simple way to generate new and/or modify existing
OKS database files. The user does not have to know about
the 'pm' package or how to access OKS databases and objects,
how to save them etc. All this is done by the main tool 'pmaker'.

Instead the user writes simple Python statements in an environment
that makes the OKS database objects available in an intuitive way.
User code lives in a file with suffix '.pm'. Although they are technically
Python scripts they can't be executed outside the special 'pmaker'
environment, hence the different suffix. Putting a special marker
for emacs in the first line tells emacs their real type.

######################## temporary ####################

How to get it:

# setup tdaq-05-03-00 or later, or nightly


getpkg DAQ/DataFlow/pmaker
cd pmaker/cmt
cmt config
cd ..

export TDAQ_PMPATH=`pwd`/modules

######################################################

Let's start with a simple example (the ------ are the file delimiters
and don't belong to the source). Copy the following into a local file, 
let's say mytest.pm

------------ mytest.pm --------------
# -*- Python -*-

include('daq/schema/core.schema.xml')

--------------------------------------

% ./pmaker -n -d MyTest.data.xml mytest.pm

Congratulations, you just created an (empty) OKS data file with the includes
for the core schema. You can call include() with both schema and data files,
and in each module you should include everything that you need directly. There is
no harm in including an already included file, since it will be ignored.

You can call 'pmaker' with the -h option to see all parameters:

---------------------------------------------------------------------------
% ./pmaker -h
usage: pmaker [-h] [-n] [-D key=value] -d DATABASE module [module ...]

Run a set of user partition maker scripts against an OKS database

positional arguments:
  module                the PM user module to run

optional arguments:
  -h, --help            show this help message and exit
  -n, --new             create a new OKS database
  -D key=value, --define key=value
                        define a context variable
  -d DATABASE, --database DATABASE
                        the OKS database to process

The OKS database can contain the prefix rdbconfig: in which case the name is
interpreted as a RDB database, e.g. rdbconfig:RDB_RW@ATLAS
------------------------------------------------------------------------------

Now we can create our first objects:

----------------------- mytest.pm -----------------------------------------
# -*- Python -*-

include('daq/schema/core.schema.xml')
include('daq/segments/setup.data.xml')

# create a new OKS object
Partition('TDAQ')

# Modify a relation
Partition.TDAQ.OnlineInfrastructure = OnlineSegment.setup

---------------------------------------------------------------------

If you run the same command as above, you now have a file that also contains a
Partition object called 'TDAQ'. 

What happens here is the following: Once you include a given schema file, 
all classes from this file are made available in the current environment
with their OKS name. In this example, 'Partition' and 'OnlineSegment'. 
You can access objects of a given class by simply referencing them inside their 
class object with a dot: 'Partition.TDAQ' is the OKS object of type 
'Partition' and UID 'TDAQ'. You probably guessed how you can access an attribute
or relation: again just use the dot notation.

To create a new object you call the name of the class as if it were a function
and pass it the desired UID (i.e. the name of the object). This is how we
created the TDAQ partition. Note that these constructors take an additional
keyword list which you can use to initialize the attributes of the object. 
We could have written the above as well in one line:

Partition('TDAQ', OnlineInfrastructure=OnlineSegment.setup)

The magic objects that represent the OKS classes have a few other capabilities:

* You can check if an object already exists with:

---------------------------------------------------------------------
if not 'TDAQ' in Partition:
   Partition('TDAQ')
else:
   # nothing to do
   pass

----------------------------------------------------------------------

* You can iterate over objects of a given class or see how many there are

----------------------------------------------------------------------
# print all partitions
for p in Partition:
   print p.id

# number of partitions
print len(Partition)

# find all segments starting with 'HLT'
hlt_segments = [ s for s in Segment if s.id.startswith('HLT') ]
print hlt_segments

----------------------------------------------------------------------

* Finally, if the object name is not a valid Python identifier, or you
  have to find an object dynamically (e.g. the name is given by a 
  parameter or variable), you can use the map interface instead:
--------------------------------------------------------------------

# same as Partition.TDAQ].OnlineInfrastructure = OnlineSegment.setup
Partition['TDAQ'].OnlineInfrastructure = OnlineSegment.setup

Segment['crazy-name'].IsControlledBy = RunControlTemplateApplication.DefRC

----------------------------------------------------------------------

The required command line arguments are the database name and at least one module
name. The -n option will overwrite the given database with a new file, otherwise
the modules will work on an existing database. Note that a valid module name is '-'
which is interpreted as sys.stdin. 

So for changing the LogRoot attribute on the partition we just created, you can say
(without the -n option !):

% echo 'Partition.TDAQ.LogRoot = "/logs/"' | ./pmaker -d MyTest.data.xml -

Oh, and we need some Tag objects to tell the partition which binaries to use:

% echo 'Partition.TDAQ.DefaultTags = [ Tag["x86_64-slc6-gcc47-opt"] ]' |  ./pmaker -d MyTest.data.xml -

Note the use of the map notation to refer to an object with a non-Pythonic identifier.

#######################################################################
Caveat: At the moment the following does *not* work:

   Partition.TDAQ.DefaultTags += [ Tag["x86_64-slc6-gcc47-dbg"] ]

but this does:

   tags = Partition.TDAQ.DefaultTags
   tags += [ Tag["x86_64-slc6-gcc47-dbg"] ]
   Partition.TDAQ.DefaultTags = tags

The first version will look alright until the object is written out to
disk. If the object has not been modified in some other way, the changes
will be lost. This can be tricky to debug since the effect depends on other
changes you might or might not make to the same object.

######################################################################

You can pass more than one module to the 'pmaker' executable, and it will run them all in
sequence. Later modules will see the changes from the earlier ones. 

./pmaker -n -d MyTest.data.xml mod1.pm mod2.pm mod3.pm

All command line parameters can also be stored in a file and referenced by @filename
The effect is as if the file is inserted at this place into the command line.

modules.txt
-----------
mod1.pm
mod2.pm
mod3.pm
-----------

% ./pmaker -n -d MyTest.data.xml @modules.txt

For tasks you do over and over again, it is advisable to put everything (including options)
into a single file. Otherwise you may forget (or add) a critical option like -n.

% ./pmaker @myproject

============================================================================================

Partitions are generated with a wide range of possible options. The pm_* scripts try to
accommodate most of them, which makes the code complicated and difficult to extend for 
new situations. One of the most difficult problems is that there are many possible dimensions 
of configurations that are quasi independent of each other. Here are some examples:

* Is the partition meant for a desktop, lxplus, test bed, preseries, Point 1, or a user specific site ?
  Parameters like network addresses and masks change, as well as default machines to be used for
  run control and monitoring, e.g.

* Is this a localhost partition (everything running on one machine) or a multi host partition ?
  Note that this is often independent from the first question.

* Does this partition include a real ROIB, HLTSV, DCM, ROS, SFO or emulators ? 
  Again this is largely independent from the first two items, and there are a multitude of
  possible combinations in this area alone.

* Do we run in internal, preloaded, 'real' mode ? 
  See above, and this affects potentially multiple applications in the system, e.g.
  for pre-loaded both HLTSV and ROS have to change, data files have to be provided etc.

The modular nature of this tool makes it possible that people write a small part of the generation
for the system they are familiar with. There are several possible ways how one can achieve the 
customization that we saw above is necessary.

1. Have extra modules that modify the generated partition after the fact.
   This is similar to the system as it is currently used at Point. With 'pmaker' it is
   as easy as adding another module at the end:

   ./pmaker @myproject p1adapt.pm

   The assumption being that 'p1adapt.pm' will make all necessary changes that are needed
   for Point 1. Apart from the fact that after a while these post-production scripts tend to rewrite
   a large part of what was generated in the first place, it also puts a lot of knowledge
   into this script: it still has to deal with localhost vs. multihost, etc. A simple example
   is the question which partition the tool should modify ? It might not be called 'ATLAS'
   if this is a local partition to test a new pre-scale set.

   Still, this is probably the prefered way for a user to make any last-minute adjustments
   and fine-tunings for a working partition.

2. The 'pmaker' tool provides a second way of customization: in the environment in which
   the pm modules run exists a dictionary called 'context'. It is used both for 
   passing a few parameters from the main program (and the command line) to the user 
   modules, as well as a way to parameterize the configuration without knowing too much
   about the details of UIDs, structure of the partition etc.

   We start with the first use to make things clearer: our original script produced a 
   partition with a fixed name 'TDAQ', some tags, and a DefaultHost that was empty.
   We did not create any data flow parameters, and we did not set the data flow 
   network parameters to any meaningful values. The idea is that these customizations
   are left to some other script to specify them.

   Let's start with the partition name. It is obviously not very practible to hard-code
   it, and let the user change it everytime he wants a new partition. There should be 
   a way to pass such information to the module. The 'context' dictionary can be used
   for simple cases like this in the following way:

   The 'pmaker' main program fills the 'context' dictionary with 
     * 'dbname' - the name of the OKS database, without '.data.xml'
     *  Any key,value pair that was defined on the command line with -Dkey=value
        'value' should only be simple strings

   Here is our new mytest.pm example:

------------------------- mytest.pm -------------------------------
# -*- Python -*-

include('daq/segments/setup.data.xml')
include('daq/schema/df.schema.xml')

partition = context.get('partition', None) or context['dbname']

DFParameters('default')
Partition(partition, 
          OnlineInfrastructure=OnlineSegment.setup, 
          DataFlowParameters=DFParameters.default)

----------------------------------------------------------------------

When we run 

% ./pmaker -n -d MyTest.data.xml mytest.pm

and check the generated data file, we'll see that the partition is now called 'MyTest'.
This is just a useful default, but often what we want anyway. In the code we can
see that we also look for a 'partition' key, that obviously does not exist. The way
we can set it and override the 'dbname' as partition name is like this:

% ./pmaker -n -d MyTest.data.xml -Dpartition=MyPartition mytest.pm

You will see that the partition is now called 'MyPartition'. While passing arbitrary
keys into the environment may seem tempting, it should be the exception rather than the rule.
Otherwise there is the danger of passing an infinite number of 'flags' into the 
modules that are used to make decisions.

We'll now use the partition example to come to the second use of the 'context'
variable. We have not set at all several important variables, e.g. the DefaultHost
of the partition. This is fine for a local partition but not for running on a testbed,
pre-series or a Point 1. Another example is the DefaultDataNetworks parameter for the
DFParameter object. It should be set to well-defined values for some systems, and
may be unpredictable for others (e.g. if running at a remote site). The 'pmaker'
way of solving this is leaving this information to a site-specific module and then
update the objects we want to modify at a later stage.

Let's take the DefaultHost first. At Point1 we want to make pc-tdq-onl-80 the default
host. In a localhost partition it should be the local host. And so on for the other
sites. Furthermore we don't know the name of the partition we are working with in advance,
so to do this in a post-generation script would need more parameters.

A site-specific script like 'site/point1.pm' could look like the following:

------------------ site/point1.pm --------------------------------------------
# -*- Python -*-

include('daq/schema/core.schema.xml')
include('daq/hw/hosts.data.xml')

extend(context, {
    'HLTSVApplication' : {
        'RunsOn': Computer['pc-tdq-dc-02.cern.ch']
    },
    'Partition': {
        'LogRoot': '/logs/${TDAQ_VERSION}',
        'WorkingDirectory': '/logs/${TDAQ_VERSION}' ,
        'DefaultHost': Computer['pc-tdq-onl-80.cern.ch'],
        'DataFlowParameters': {
            'DefaultDataNetworks': [ '10.147.0.0/255.255.0.0', '10.150.0.0/255.255.0.0' ],
	    'MulticastAddress': '224.100.1.2/10.147.0.0' 
        }
    },
    'HLTSegment': {
         'Racks': [ rack for rack in Rack if rack.startswith('rack-ef-') or rack.startswith('rack-xpu-') ]
    }
})
-------------------------------------------------------------------------------

All this script does is store some some site specific overrides in the 'context' dictionary.
Note that this does not even create new objects, although that would not be forbidden. Basically it 
just stores some preferences in a place where other modules can find it. The top level keys are 
freely chosen, while the keys further down are typically interpreted as attribute or relation names.

What would a module do with such information ? Mainly, it would not not care about the details, but
simply call:

update(Partition[partition], context.get('Partition', {}))

which would be the last line in our script generating the partition.

Note that the site specific module has no need to know the name of the partition or any other details.
Note how the module calling update() makes it optional that anybody has defined any overrides by
giving the empty dictionary as default. The update() call would modify the partition object passed
in according to the information in the 'context' dictionary.

In a similar way, if there was a HLTSVApplication defined somewhere, the corresponding module would
call 

update(HLTSApplication.HLTSV, context.get('HLTSVApplication', {}))

Again there is no need to know the names of the application or anything else. The 'HLTSegment'
key would be used by the HLT segment module to know about the racks to add to its TemplateSegment.

To see the other extreme, here is how the localhost.pm module could look like:

-------------------------- localhost.pm ------------------------------
# -*- Python -*-

include('daq/schema/core.schema.xml')

import socket
import pm.farm

me = socket.getfqdn()
if not me in Computer:
   localhost = pm.farm.network_computer(me)[0]
   db.addObjects([localhost])
else:
   localhost = Computer[me]

Rack('local')
Rack.local.LFS = [ localhost ]
Rack.local.Nodes = [ localhost, localhost ]

extend(context,({
    'Partition': { 
        'DefaultHost': localhost
    },
    'HLTSVApplication' : {
        'RunsOn': None
    },
    'HLTSegment' : {
        'DefaultHost': None,
        'Racks': [ Rack.local ]
    }
}))
-----------------------------------------------------------------------

This is probably using more black magic than you normally need. It first checks
if the localhost is already known. If you've included a site specific module, it may
have loaded the appropriate hardware database. If not, it generates a new Computer
object on the fly. Then a dummy Rack object is generated with only the local host,
that can serve as a HLT segment. The local host is also made the default host 
of the partition.

Note that we have not modified the DataFlowParameters in this case. The assumption
is that this is done by a site specific module and is orthogonal to the fact that
everything should just run on a single host. So you could execute:

% ./pmaker -n -d MyTest.data.ml site/point1.pm localhost.pm hltsv.pm hlt.pm sfo.pm partition.pm

and get a localhost HLT partition but setup for Point 1. 

Even if you are running in a completely non-standard site, the site/standalone.pm module
should give you some sensible defaults.

In most cases you should just include the 'site.pm' module, which will try to determine
the site at run-time and then load the appropriate module itself. You can override
this selection by passing -Dsite=SITENAME on the command line. This is useful for testing
e.g. a Point1 configuration without being at Point 1. In this case your TDAQ_DB_PATH should
reflect that and point to a local copy of the P1 oks databases.

========================  Standard Modules ===================================================

The pre-defined .pm modules live in a directory 'modules' in the 'pmaker' package.
You should define TDAQ_PMPATH to point to that directory, and - if desired - to any
other areas where you might have local definitions or want to override them.

export TDAQ_PMPATH=/my/modules:/path/to/pmaker/modules

site.pm

   This module should be included first by most projects. It will determine the current
   site and include another specific module from site/<NAME>.pm where NAME is currently
   one of 'point1', 'testbed', 'preseries', 'lxplus' and 'standalone'.

   This module also includes the localhost.pm module (see below on how to activate it).

   You can define you own site/somewhere.pm module and override the auto-detection on 
   the command line with: -Dsite=NAME

localhost.pm

   This module is automatically included by site.pm, so there is no need to specify it
   explicitly. However, it is only activated when you specify -Dlocalhost=yes on the command line,
   otherwise it is ignored. You can also explicitly include it in your list of modules, then
   it will be always active.

site/standalone.pm

   The site specific module that is chosen if nothing else matches. It will try to determine 
   the local IP address and netmask - it may fail for the latter and print a message to
   remind the user to fix it by hand in the generated partition. You can force the use of this
   module by -Dsite=standalone, in case the auto-detection does not work.

site/point1.pm

   The Point 1 specific site module.

site/testbed.pm

   The Lab 4 testbed specific site module.

site/preseries.pm

   The preseries specific site module.

datafiles.pm

   This module generates a list of DataFile objects based on user input. A single data file
   can be specified with -Ddatafile=/path/to/rawfile, a list files should be put into a text
   file and referenced with -Ddatafiles=/path/to/listfile

hltsv/internal.pm
hltsv/preloaded.pm
hltsv/filar.pm
hltsv/ttc2lan.pm

   These four modules specify one of the valid RoIBPlugins for the HLT supervisor. Choose one
   to select the method.

hltsv/dummy.pm

   This module creates a HLT and ROS segment with the HLTSV specific DCM and ROS dummy applications.
   This is only needed for HLTSV specific tests. 

   See examples/hltsv_only on how to combine these.

hltsv.pm

   The main HLTSV module, it generates the HLTSV, applies the modifications for the RoIBPlugin and
   arranges for the supervisor to run in the HLT segment. It also defines local monsvc configurations
   to publish only to the top level DF and Histogramming servers.


dcm/l1source/dummy.pm
dcm/l1source/hltsv.pm

   Choose one of these modules to select the DCM input source.

dcm/processor/dummy.pm
dcm/processor/hltpu.pm

   Choose one of these modules to select the DCM HLTPU implementation (dummy or real).

dcm/datacollector/dummy.pm
dcm/datacollector/ros.pm

   Choose one of these modules to select the data collection in the DCM (dummy or real).

dcm/output/file.pm
dcm/output/efio.pm
dcm/output/sfo.pm

   Choose one of these modules to select the DCM output (file, SFO via EFIO, SFO via asyncmsg).

   See examples/dcm_only and examples/hltsv_dcm on how to combine these.

hltpu/dummy.pm
hltpu/dcm.pm

   Choose on of these as the DF interface for the HLTPU (dummy or DCM)

hltpu.pm

    This modules creates the HLTRC and HLTMPPU applications and prepares for them to
    be available in the HLT template segment.

gatherer.pm

   This module will add the default DF gatherers into the HLT segments.

monsvc.pm

   This module defines a default DF publication configuration. You should not
   use this explicitly, but use require('monsvc.pm') instead from one of your
   modules if you need this. It will only be included once in this case. It 
   stores the configuration as 'DFMonsvc' in the context object.

sfo.pm
    Create SFO applications on site specific hosts.

hlt.pm
   This module will pull all the HLTSV, DCM, HLTPU, gatherer, SFO etc. components together
   and create the actual segments.

ros/emulated.pm
ros/preloaded.pm
ros/include.pm

    Choose one of these to either generate a single emulated ROS, a preloaded ROS (requires
    that datafiles are specified) or include a pre-generated ROS segment. The latter requires
    the -Dros-segment=<path/to/segment.data.xml> command line option.

ros.pm
    Create the main ROS applications and the ROS segment.

trigger/pudummy.pm
trigger/joboptions.pm

    Choose one of these to set the TriggerConfiguration in the main partition.

partition.pm

    Creates the main partition object. By default it will use the name of the OKS data file
    as partition name. This can be overridden with the -Dpartition=NAME command line.

============================== Examples ==================================================

These are all in the examples/ subdirectory.
You can generate these with:

   % pmaker -d mytest.data.xml @examples/hltsv_only

Typical options are:

     -Dlocalhost=1                   for a localhost version
     -Ddatafile=/path/to/rawfile     if you need pre-loaded data, or
     -Ddatafiles=/path/to/listfile
     -Dsite=standalone               if you are on a non-standard system
     -Dpartition=NAME                if you want a different partition name.
     -Dros-segment=/path/to/segment  if you want to include a pre-generated ROS segment 
                                     (use ros/include.pm instead of emulated or preloaded)

hltsv_only
        HLTSV plus dummy DCM and ROS apps.

hltsv_dcm
        HLTSV plus DCM using dummy for DC, output, processor.

hltsv_dcm_sfo
        HLTSV plus DCM plus SFO, DCM using dummy for DC and processor.

dcm_only
        DCM only with all dummies.

hltpu_only
        HLTPU only with dummy DF interface.

ros_only
        ROS segment only generation

hlt_segment
        HLT segment only generation 

full_partition
        Full partition (HLTSV, DCM, HLTMPPU, SFO, ROS)

